import { Injectable } from "@angular/core";
import { UserSession } from "./user.session";

@Injectable()
export class AppSession {
    constructor(
        private userSession: UserSession
    ) {

    }

    get User(): UserSession {
        return this.userSession;
    }


}