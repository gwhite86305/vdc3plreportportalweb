import { CanActivate, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { UserSession } from "./user.session";

@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor (
        private router:Router,
        private userSession: UserSession){
        
    }
    canActivate(){
        var isLoggedIn = this.userSession.isSignedIn;
        if (!isLoggedIn)
            this.router.navigate(['/home']);
        return isLoggedIn;
    }
}