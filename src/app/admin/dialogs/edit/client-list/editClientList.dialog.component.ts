import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {ClientListService} from '../../../services/client-list.service';
import {ClientKeyService} from '../../../services/clientkey.service';
import {FormControl, Validators} from '@angular/forms';
import {Router } from '@angular/router';

@Component({
  selector: 'app-edit-client-list-dialog',
  templateUrl: 'editClientList.dialog.html',
  styleUrls: ['editClientList.dialog.css']
})
export class EditClientDialogComponent implements AfterViewInit {

  constructor(public dialogRef: MatDialogRef<EditClientDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,  public dataService: ClientListService, public myService: ClientKeyService, private router: Router) {
            this.myService.myMethod(this.data.client_key);
         }
 
  public clientdata: string;

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngAfterViewInit() {
     console.log('edit Client Dialog ngAfterViewInit client key is' + this.data.client_key);
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // empty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


 // stopEdit(): void {
 //   this.dataService.updateClient(this.data);
 // }

}
