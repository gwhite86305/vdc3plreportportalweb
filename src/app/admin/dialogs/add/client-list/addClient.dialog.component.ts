import {MAT_DIALOG_DATA, MatDialogRef, MatDialogModule } from '@angular/material';
import { Component, Inject} from '@angular/core';
import { ClientListService } from '../../../services/client-list.service';
import { FormControl, Validators} from '@angular/forms';
import { PartnerModel } from '../../../interfaces/partner.interface';

@Component({
  selector: 'app-add-client.dialog',
  templateUrl: 'addClient.dialog.html',
  styleUrls: ['addClient.dialog.css']
})

export class AddClientDialogComponent {
  constructor(public dialogRef: MatDialogRef<AddClientDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: PartnerModel,
              public dataService: ClientListService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('title') ? 'Not a valid title' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

 // public confirmAdd(): void {
 //   this.dataService.addClientPartner(this.data);
 // }
}
