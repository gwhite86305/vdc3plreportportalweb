import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ClientListService } from '../../services/client-list.service';
import { HttpClient } from '@angular/common/http';
//import { MatTableModule } from '@angular/material';
//import { MatDialog, MatIconModule, MatPaginator, MatSort } from '@angular/material';
import { Observable} from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
//import { DataSource} from '@angular/cdk/collections';
//import 'rxjs/add/observable/merge';
//import 'rxjs/add/observable/fromEvent';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/debounceTime';
//import 'rxjs/add/operator/distinctUntilChanged';
import { first } from 'rxjs/operators';

import { PartnerModel } from '../../interfaces/partner.interface';
import { PartnerService } from 'src/app/admin/services/partner.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { ReportSortModel } from 'src/app/portal/models/report.model';
import { EntityInfoModel } from 'src/app/shared/interfaces/entityInfo.interface';

//import { AddClientDialogComponent } from '../../dialogs/add/client-list/addClient.dialog.component';
//import {EditClientDialogComponent} from '../../dialogs/edit/client-list/editClientList.dialog.component';
//import { EntityInfoModel } from 'src/app/shared/interfaces/entityInfo.interface';

const CLASS_NAME: string = 'PartnerListComponent'; //used for logging to console

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.css']
})
export class PartnerListComponent implements OnInit {
  entityInfo: EntityInfoModel = {
    name: 'Partner',
    namePlural: 'Partners',
    url: '/admin/partner'
  };
  dataRows: Array<PartnerModel>;
  dataRowsFiltered: Array<PartnerModel>;
  filterVal: string = '';
  sort: ReportSortModel = { colName: '', isDescending: false };
  //isLoaded: boolean = false;
  //displayedColumns = ['id', 'name', 'description', 'userCount', 'dateUpdated', 'actions'];
  //ClientDatabase: ClientListService | null;
  //dataSource: ClientDataSource | null;
  //index: number;
  //id: number;
  //isDisabled: boolean = true; 
  //Client_key: string;

  constructor(
    //public httpClient: HttpClient,
    //public dialog: MatDialog,
    //public dataService: ClientListService,
    private svc: PartnerService,
    private svcUtility: UtilityService
  ) { }

  //@ViewChild(MatPaginator) paginator: MatPaginator;
  //@ViewChild(MatSort) sort: MatSort;
  //@ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  /*
  refresh() {
    this.loadData();
  }

  addNew(client: ClientModel) {
  //  const dialogRef = this.dialog.open(AddClientDialogComponent, {
  //    data: {client: client }
  //  });

  //  dialogRef.afterClosed().subscribe(result => {
  //    if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside PartnerTableService
  //      this.ClientDatabase.dataChange.value.push(this.dataService.getDialogData());
  //      this.refreshTable();
  //    }
  //  });
  }

  startEdit(i: number, Id: number, Name: string, Description: string, UserCount: string, DateCreated: string, 
            DateUpdated: string, IsActive: boolean) 
  {
    //          this.id = Id;
    // index row is used just for debugging proposes and can be removed
    //          this.index = i;
    //          console.log(this.index);
            //  const dialogRef = this.dialog.open(EditClientDialogComponent, {
            //      data: {id: Id, Name: Name, Description: Description, UserCount: UserCount,DateCreated: DateCreated, 
            //      DateUpdated: DateUpdated, IsActive: IsActive  }
            //  });

      //      dialogRef.afterClosed().subscribe(result => {
      //          if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
                    const foundIndex = this.ClientDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
                    this.ClientDatabase.dataChange.value[foundIndex] = this.dataService.getDialogData();
        // And lastly refresh table
      //              this.refreshTable();
      //          }
      //      });
  }

  // If you don't need a filter or a pagination this can be simplified, you just use code from else block
  private refreshTable() {
    // if there's a paginator active we're using it for refresh
 //   if (this.dataSource._paginator.hasNextPage()) {
 //     this.dataSource._paginator.nextPage();
 //     this.dataSource._paginator.previousPage();
 //     // in case we're on last page this if will tick
 //   } else if (this.dataSource._paginator.hasPreviousPage()) {
 //     this.dataSource._paginator.previousPage();
 //     this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
 //   } else {
 //     this.dataSource.filter = '';
 //     this.dataSource.filter = this.filter.nativeElement.value;
 //   }
  }
*/
  
  public loadData() {

    this.svcUtility.setSystemProcessing(true);
    this.svc.getItems_All()
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data;
        this.dataRowsFiltered = data;
        //initial sort
        this.sortColumn('name');
        this.svcUtility.setSystemProcessing(false);
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.setSystemProcessing(false);
      });

    /*
    this.ClientDatabase = new ClientListService(this.httpClient, this.svcUtility);
    this.dataSource = new ClientDataSource(this.ClientDatabase, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
      */
  }

  //on filter click, also triggered on key up
  onFilter(val) {
    this.svcUtility.logMessage('on Filter: ' + val, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    //get copy of original and then apply filter to it.
    var filtered: Array<PartnerModel> = JSON.parse(JSON.stringify(this.dataRows));
    //if there is a value in filtered, perform the filtering
    if (val.length > 0) {
      // Filter data
      filtered = filtered.filter((client: PartnerModel) => {
        var searchStr = (client.name.toLowerCase() + '|||' + client.description.toLowerCase());
        return searchStr.indexOf(val.toLowerCase()) !== -1;
      });
    }
    //update the filtered array
    this.dataRowsFiltered = filtered;
  }

  //sort
  sortColumn(colName: string) {
    this.svcUtility.logMessage('sortColumn:' + colName, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    var result = this.svcUtility.sortColumn(this.sort, this.dataRowsFiltered, colName);
    this.dataRowsFiltered = result.data;
    this.sort = result.sort;
  }
}


////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
/*
export class ClientDataSource extends DataSource<ClientModel> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: ClientModel[] = [];
  renderedData: ClientModel[] = [];

  constructor(public _clientDatabase: ClientListService, public _sort: MatSort)
  { 
    super();
    // Reset to the first page when the user changes the filter.
   // this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  //Connect function called by the table to retrieve one stream containing the data to render. 
  connect(): Observable<ClientModel[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._sort.sortChange,
      this._clientDatabase.dataChange,
      this._filterChange,
  //    this._paginator.page
    ];

    this._clientDatabase.getAllClients();

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
      this.filteredData = this._clientDatabase.data.slice().filter((client: ClientModel) => {
        const searchStr = (client.id + client.name + client.description).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
    //  const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(0, 10);
      return this.renderedData;
    });
  }
  disconnect() {
  }
  //Returns a sorted copy of the partner database data.
  sortData(data: ClientModel[]): ClientModel[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
*/

