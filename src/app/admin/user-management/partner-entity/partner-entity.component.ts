import { Component, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { PartnerModel, PartnerNew, PartnerSelectOne, TplPartnerModel } from 'src/app/admin/interfaces/partner.interface';
import { PartnerService } from 'src/app/admin/services/partner.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { selectValidator } from 'src/app/shared/validators/form.validators';

var CLASS_NAME: string = "PartnerEntityComponent";

@Component({
  selector: 'partner-entity',
  templateUrl: './partner-entity.component.html',
  styleUrls: ['./partner-entity.component.scss']
})

@Injectable()
export class PartnerEntityComponent {
  frmEdit: FormGroup;
  unassignedPartners: Array<TplPartnerModel> = [{id:null, partnerName: null}];  //only populate in add scenario, put in a single --select one--
  item: PartnerModel = JSON.parse(JSON.stringify(PartnerNew));
  submitted = false;
  error = '';
  isLoaded: boolean = false;

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private svc: PartnerService,
    private svcUtility: UtilityService
  ) {
  }

  // convenience getter for easy access to form fields
  get f() { return this.frmEdit.controls; }

  ngOnInit() {
    this.initFormGroup(); //call this early w/ empty model for proper initialization
    var id = this.route.snapshot.params["id"];
    this.loadData(id);
  }

  loadData(id: any) {
    console.log("edit partner");
    if (id != null) { //only for edit mode
      //load partner item 
      this.svc.getItem_ById(id)
        .pipe(first())
        .subscribe(
        data => {
          this.item = data;
          this.initFormGroup(); //call this early w/ empty model for proper initialization
          this.isLoaded = true;
        },
        error => {
          this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
          this.isLoaded = true;
        });
    }
    //load partner item lookup 
    if (this.item.id == null) { //only for add mode
      this.svc.getItems_AllUnassigned()
        .pipe(first())
        .subscribe(
        data => {
          this.unassignedPartners = data;
          this.unassignedPartners.splice(0, 0, JSON.parse(JSON.stringify(PartnerSelectOne)));
          this.isLoaded = true;
        },
        error => {
          this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
          this.isLoaded = true;
        });
    }
  }

  initFormGroup() {
    if (this.item.id == null) {
      this.frmEdit = this.formBuilder.group({
        tplPartnerID: [this.item.tplPartnerID, selectValidator]
        , description: [this.item.description]
        , active: [this.item.active]
      });
    }
    else {
      this.frmEdit = this.formBuilder.group({
          description: [this.item.description]
        , active: [this.item.active]
      });
    }

  }

  onSubmit()
  {
    this.svcUtility.logMessage('onSubmit', CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    this.error = '';
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmEdit.invalid) {
      return;
    }

    //apply vals back to the model
    if (this.item.id == null) { //only for add mode
      this.item.tplPartnerID = this.frmEdit.controls.tplPartnerID.value;
    }
    this.item.description = this.frmEdit.controls.description.value;
    this.item.active = this.frmEdit.controls.active.value;
    //get name value for post save message
    if (this.item.id == null) {
      this.unassignedPartners.some((item: TplPartnerModel) => {
        if (item.id == this.item.tplPartnerID) this.item.name = item.partnerName;
        return item.id == this.item.tplPartnerID;
      });
    }

    //validate
    //call service save
    this.svc.saveItem(this.item)
      .pipe(first())
      .subscribe(
      data => {
        this.svcUtility.displayMessage("Partner '" + this.item.name + "' was saved.", this.svcUtility.LogMessageSeverity.Info);
        this.router.navigate(['/admin/partner/list/']);
      },
      error => {
        this.svcUtility.displayMessage(this.svcUtility.translateHTTPError(error), this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
      });
  }
}
