import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { first } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { UserService } from 'src/app/shared/services/user.service';
import { UserModel } from 'src/app/portal/models/users';
import { EntityInfoModel } from 'src/app/shared/interfaces/entityInfo.interface';
import { ReportSortModel } from 'src/app/portal/models/report.model';

const CLASS_NAME: string = 'UserListComponent'; //used for logging to console

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  entityInfo: EntityInfoModel = {
    name: 'User',
    namePlural: 'Users',
    url: '/admin/user/'
  };

  dataRows: Array<UserModel>;
  dataRowsFiltered: Array<UserModel>;
  filterVal: string = '';
  sort: ReportSortModel = { colName: '', isDescending: false };

  constructor(
    private route: ActivatedRoute,
    private svc: UserService,
    private svcUtility: UtilityService
  ) { }

  ngOnInit() {
    var partnerId = this.route.snapshot.params['partnerId'];
    
    this.loadData(partnerId);
  }

  public loadData(partnerId: any) {

    this.svcUtility.setSystemProcessing(true);
    this.svc.getItems_ByPartnerId(partnerId)
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data;
        this.dataRowsFiltered = data;
        //initial sort
        this.sortColumn('name');
        this.svcUtility.setSystemProcessing(false);
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.setSystemProcessing(false);
      });
  }

  //on filter click
  onFilter(val) {
    this.svcUtility.logMessage('on Filter: ' + val, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    //get copy of original and then apply filter to it.
    var filtered: Array<UserModel> = JSON.parse(JSON.stringify(this.dataRows));
    //if there is a value in filtered, perform the filtering
    if (val.length > 0) {
      // Filter data
      filtered = filtered.filter((client: UserModel) => {
        var searchStr = (client.firstName.toLowerCase() + '|||' + client.lastName.toLowerCase());
        return searchStr.indexOf(val.toLowerCase()) !== -1;
      });
    }
    //update the filtered array
    this.dataRowsFiltered = filtered;
  }

  //sort
  sortColumn(colName: string) {
    this.svcUtility.logMessage('sortColumn:' + colName, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    var result = this.svcUtility.sortColumn(this.sort, this.dataRowsFiltered, colName);
    this.dataRowsFiltered = result.data;
    this.sort = result.sort;
  }

}

