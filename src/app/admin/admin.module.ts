import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTableModule, MatDialog, MatDialogModule, MatIconModule, MatPaginator, MatSort, MatSortModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { PartnerListComponent } from './user-management/partner-list/partner-list.component';

import { AddClientDialogComponent} from '../admin/dialogs/add/client-list/addClient.dialog.component';
import { EditClientDialogComponent} from '../admin/dialogs/edit/client-list/editClientList.dialog.component';

import { PartnerEntityComponent } from 'src/app/admin/user-management/partner-entity/partner-entity.component';
import { ManageRolesComponent } from './user-management/manage-roles/manage-roles.component';
import { ClientListService } from '../../app/admin/services/client-list.service';
import { ClientKeyService} from '../admin/services/clientkey.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { UserListComponent } from 'src/app/admin/user-management/user-list/user-list.component';
import { UserEntityComponent } from 'src/app/admin/user-entity/user-entity.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    NgbModule,
    MatTableModule,
    MatSortModule,
    AdminRoutingModule,
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
      AdminComponent
    , PartnerListComponent
    , AddClientDialogComponent
    , EditClientDialogComponent
    , PartnerEntityComponent
    , UserListComponent
    , UserEntityComponent
    , ManageRolesComponent
  ], 
  entryComponents: [
    AddClientDialogComponent,
    EditClientDialogComponent
  ],  
  providers: [ClientListService, ClientKeyService, UtilityService],
  bootstrap: [AdminComponent]
})
export class AdminModule { }
