import { Component, Injectable } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, FormArray,  ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { PartnerModel, PartnerNew, TplPartnerModel } from "src/app/admin/interfaces/partner.interface";
import { UserService } from 'src/app/shared/services/user.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { UserModel, Permission, Role, UserNew } from 'src/app/portal/models/users';

import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { map } from 'rxjs/operators'
import { of } from 'rxjs';
import { PartnerEntityComponent } from '../user-management/partner-entity/partner-entity.component';
//import { User_Mock } from '../../portal/models/user.mockdata';

var CLASS_NAME: string = "UserEntityComponent";

@Component({
  selector: 'user-entity',
  templateUrl: './user-entity.component.html',
  styleUrls: ['./user-entity.component.scss']
})

@Injectable()
export class UserEntityComponent {
  frmEdit: FormGroup;
  item: UserModel = JSON.parse(JSON.stringify(UserNew));
  partnerId: any;
  submitted = false;
  error = '';
  user: UserModel;
    
 // partner: PartnerModel = {} as IObject;
  permissions: Array<Permission>;
   
  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private svc: UserService,
    private svcUtility: UtilityService,
    private svcAuthenticate: AuthenticationService
  ) {
     ////this is not the way we should be getting currentUser - Plenty of examples elsewhere in the code of how this is done.
     ////look at partnerEntity component for proper way.
     //this.user = JSON.parse(localStorage.getItem('currentUser'));
     ////TBD - this needs to be moved - not in constructor
     ////getPermissions will make an API call to get the list of available permissions - consider getting once and storing in localStorage - it is pretty static
     ////form load should wait for this to complete prior to loading everything
     //this.permissions = this.getPermissions();
     //this.addCheckboxes();

  }

  // convenience getter for easy access to form fields
  get f() { return this.frmEdit.controls; }


  ngOnInit() {
    console.log('ngOnint');
    //SC - this is not the way we should be getting currentUser - Plenty of examples elsewhere in the code of how this is done.
    //look at partnerEntity component for proper way.

    this.user = JSON.parse(localStorage.getItem('currentUser'));

    //SC - TBD - need to get permissions by calling API. This will require a change here so we get back data
    // from lookup permissions call and then initFormGroup with that look up
    //getPermissions will make an API call to get the list of available permissions - consider getting once and storing in localStorage - it is pretty static
    //form load should wait for this to complete prior to loading everything

    this.permissions = this.user.permissions;
    this.initPartnerId(this.user);
    this.initFormGroup();

   /*
   this.svc.getPermissions_All()
   .pipe(first())
   .subscribe(
   data => {
     this.permissions = this.data;
     this.initFormGroup(); //call this early w/ empty model for proper initialization
   },
   error => {
     this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
   });
   */

    this.addCheckboxes();

    //2 ways to get to this form - admin edit existing user or admin add new user
    if (this.route.snapshot.url.join('/').indexOf('/new') > -1) {
    }
    else
    {
      const id = this.route.snapshot.params["id"];
     
      this.loadData(id);
    }
  }

  /// Protect how we assign partner id and how we return to manage users list. 
  ///   if user is system admin, then it is ok to use partnerId from query string
  ///   if user is partner admin, get partner Id from current user profile 
  initPartnerId(user: UserModel)
  {
    this.partnerId = this.user.partner.id;  //TODO: temp
    //if () {
     //   this.partnerId = this.user.partnerID;
    //}
    //else {
      //TODO - check role, use current user role and pull partnerId from current role. 
    //  this.partnerId = this.route.snapshot.params["partnerId"];
    //}
    
  }

  loadData(id: any) {

    this.svc.getItem_ById(id)
      .pipe(first())
      .subscribe(
      data => {
        //TODO: if user partner id is not the same for that found in data, then throw exception
        this.item = data;
        this.initFormGroup(); 
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
      });
  }

  initFormGroup() {
    //SC - move this because we need it onInit but also in edit scenario once we get the data
    this.frmEdit = this.formBuilder.group({
      username: [this.item.username, Validators.required]
      , email: [this.item.email, Validators.required]
      , firstName: [this.item.firstName, Validators.required]
      , lastName: [this.item.lastName, Validators.required]
      , permissions: new FormArray([], this.minSelectedCheckboxes(1))
      , active: this.item.active
      , locked: this.item.active
    });
  }

  private addCheckboxes() {
    this.permissions.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.frmEdit.controls.permissions as FormArray).push(control);
    });
  }

  //SC - get permissions should be getting the list of permissions from the API. The current user is an admin and the new
  // user would not be assigned current user's permissions.
    
  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);
  
      return totalSelected >= min ? null : { required: true };
    };
  
    return validator;
  }

  onSubmit() {
    let selectedpermissions: Array<Permission> = new Array();
    let roles: Array<Role> = new Array(); 
    let unassignedPartners: PartnerModel = {id: this.partnerId, tplPartnerID: this.partnerId, name: ' ', description: ' ', userCount: 0, dateCreated: new Date(), dateUpdated: new Date(), active: true };  
    const selectedPermissionsIds = this.frmEdit.value.permissions
      .map((v, i) => v ? this.permissions[i].id : null)
      .filter(v => v !== null);

    this.svcUtility.logMessage('onSubmit', CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);

    this.error = '';
    this.submitted = true;
    
        
    // stop here if form is invalid
    if (this.frmEdit.invalid) {
      return;
    }

    for (let i=0; i< selectedPermissionsIds.length; i++) {
      selectedpermissions[i] = this.permissions.find(obj => obj.id == selectedPermissionsIds[i]);  
    }

    //apply vals back to the model
    this.item.username = this.frmEdit.controls.username.value;
    this.item.email = this.frmEdit.controls.email.value;
    this.item.firstName = this.frmEdit.controls.firstName.value;
    this.item.lastName = this.frmEdit.controls.lastName.value;
    this.item.permissions = selectedpermissions;
    this.item.roles = this.user.roles;
    this.item.partner = unassignedPartners;
    this.item.partner.id =  this.partnerId;
    this.item.active = (this.item.id == null ? true : this.frmEdit.controls.active.value);
    this.item.locked = (this.item.id == null ? false : this.frmEdit.controls.locked.value);
    if (this.item.id == null) {
      this.item.id = this.partnerId;
    }
    //validate
    //call service save
    this.svc.saveItem(this.item)
      .pipe(first())
      .subscribe(
      data => {
        this.svcUtility.displayMessage("User '" + this.item.firstName + ' ' + this.item.lastName + "' was saved.", this.svcUtility.LogMessageSeverity.Info);
        this.router.navigate(['/admin/user/list', this.partnerId]);
      },
      error => {
        this.svcUtility.displayMessage(this.svcUtility.translateHTTPError(error), this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
      });
  }
}

