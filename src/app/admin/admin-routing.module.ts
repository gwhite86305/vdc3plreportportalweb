import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartnerListComponent } from './user-management/partner-list/partner-list.component';
import { PartnerEntityComponent } from 'src/app/admin/user-management/partner-entity/partner-entity.component';
import { ManageRolesComponent } from './user-management/manage-roles/manage-roles.component';
import { UserListComponent } from 'src/app/admin/user-management/user-list/user-list.component';
import { UserEntityComponent } from 'src/app/admin/user-entity/user-entity.component';
import { AdminComponent } from 'src/app/admin/admin.component';
import { MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSortModule,
  MatTableModule, MatToolbarModule,
} from '@angular/material';
import { CanManagePartnersGuard } from 'src/app/shared/guards/canManagePartners.guard';
 
const routes: Routes = [
  {
    path: '', component: AdminComponent,  //sets us up to use an admin layout for all child components, come back to this
    children: [
      { path: 'partner/list', component: PartnerListComponent, canActivate: [CanManagePartnersGuard] }
      , { path: 'partner/new', component: PartnerEntityComponent, canActivate: [CanManagePartnersGuard]  }
      , { path: 'partner/:id', component: PartnerEntityComponent, canActivate: [CanManagePartnersGuard]  }
      , { path: 'user/list/:partnerId', component: UserListComponent }
      , { path: 'user/new', component: UserEntityComponent }
      , { path: 'user/:id', component: UserEntityComponent }
      , { path: 'user-management/manage-roles', component: ManageRolesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), //, { useHash: true })],
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule
],  
  exports: [RouterModule],
})
export class AdminRoutingModule { }
