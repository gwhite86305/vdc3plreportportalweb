import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';
import { PartnerModel, TplPartnerModel } from 'src/app/admin/interfaces/partner.interface';

const CLASS_NAME: string = 'PartnerService'; //used for logging to console

///--------------------------------------------------------------------------
/// ClientService - Class which interacts with web API to get Partner data
///--------------------------------------------------------------------------
@Injectable({providedIn: 'root'})
export class PartnerService {

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(private http: HttpClient, private svcUtility: UtilityService) { }

  ///--------------------------------------------------------------------------
  /// Get All
  ///--------------------------------------------------------------------------
  getItems_All(): Observable<Array<PartnerModel>> {

    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'partner/getall', {})
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return data
          var result: Array<PartnerModel> = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Get all partners not assigned to partner container yet
  ///--------------------------------------------------------------------------
  getItems_AllUnassigned(): Observable<Array<TplPartnerModel>> {

    //TODO: update url
    //return this.http.get<any>(GlobalConstants.BASE_API_URL + 'partner/getunassigned', {})
    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'partner/GetTplPartners', {})
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return data
          console.log('get unassigned');
          var result: Array<TplPartnerModel> = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Get by partner id
  ///--------------------------------------------------------------------------
  getItem_ById(id: any): Observable<PartnerModel> {

    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'partner/getbypartnerid?id=' + id, { id: id })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return data
          var result: PartnerModel = res;
          return result;
        }
      }));
  }

  /////--------------------------------------------------------------------------
  ///// Save item
  /////--------------------------------------------------------------------------
  saveItem(item: PartnerModel): Observable<boolean> {

    var url: string = GlobalConstants.BASE_API_URL + 'partner/' + (item.id == undefined ? 'add' : 'update');
    if (item.id == null) item.id = -1;  //API does not like model with null id. 
    return this.http.post<any>(url, item, {responseType: 'text' as 'json'})
      .pipe(map((res: any) => {
        var result: any = null;
        if (res) {
          return true;
        }
        return false;
      }));
  }

}
