import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { PartnerModel } from '../interfaces/partner.interface';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

const CLASS_NAME: string = 'ClientListService'; //used for logging to console

@Injectable({
  providedIn: 'root'
})
export class ClientListService {

  dataChange: BehaviorSubject<PartnerModel[]> = new BehaviorSubject<PartnerModel[]>([]);
  // Temporarily stores data from add and edit dialogs
  dialogData: any;

  constructor(private http: HttpClient, private svcUtility: UtilityService) { }

  get data(): PartnerModel[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllClients(): void {
    this.http.get<PartnerModel[]>(GlobalConstants.BASE_API_URL + 'partner/getall').subscribe(data => {
      console.log('data');
      this.dataChange.next(data);
    },
    (error: HttpErrorResponse) => {
      this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
    });
  }
 

  addClient (client: PartnerModel): void {
    this.dialogData = client;
  }

  updateClient (client: PartnerModel): void {
    this.dialogData = client;
  }

 }
