import { GlobalConstants } from "src/app/shared/services/global-settings.service";

export interface PartnerModel {
  id: any;
  tplPartnerID: any;
  name: string; 
  description: string; 
  userCount: number; 
  dateCreated: Date; 
  dateUpdated: Date;
  active: boolean;
}

export const PartnerNew: PartnerModel = {
  id: null,
  tplPartnerID: null,
  name: '',
  description: '',
  userCount: 0,
  dateCreated: new Date(),
  dateUpdated: new Date(),
  active: true
}

export interface TplPartnerModel {
  id: any;
  partnerName: string;
//  gP_CustomerNumber: string;
}

export const PartnerSelectOne: TplPartnerModel = {
  id: null,
  partnerName: GlobalConstants.SelectOneCaption
}


