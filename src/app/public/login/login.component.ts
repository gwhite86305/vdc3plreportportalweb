import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { DisplayMessageService } from 'src/app/shared/services/display-message.service';

var CLASS_NAME: string = "LoginComponent";

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
      private svcAuthenticate: AuthenticationService,
      private svcUtility: UtilityService,
      private svcDisplayMessage: DisplayMessageService
    ) { }

    ngOnInit() {
        // get return url from route parameters or default to '/home'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';

        //if user is authenticated, then redirect to home. don't make them re-login
        this.svcAuthenticate.isAuthenticated.subscribe(data => {
          //if authenticated, go to return url right away
          if (data) {
            this.router.navigate([this.returnUrl]);
          }
        }).unsubscribe();

        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        //this.authenticationService.logout();

    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
      this.error = '';
      this.svcDisplayMessage.clearDisplayMessagesAll();
      this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

      this.loading = true;
      this.svcAuthenticate.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
          error => {
            this.svcUtility.displayMessage(this.svcUtility.translateHTTPError(error), this.svcUtility.LogMessageSeverity.Error);
            this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
            this.loading = false;
          });
    }
}
