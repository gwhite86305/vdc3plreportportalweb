import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from 'src/app/portal/home/home.component';
import { NotificationListComponent } from 'src/app/portal/notification-list/notification-list.component';
import { NotificationService } from 'src/app/portal/services/notification.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule
      ],
      declarations: [
        HomeComponent,
        NotificationListComponent
      ],
      providers: [NotificationService, UtilityService]
    }).compileComponents();
  }));

  it(': should create the component', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const item = fixture.debugElement.componentInstance;
    expect(item).toBeTruthy();
  });

  it(': should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent.length).toBeGreaterThan(0);
  });
});
