import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalComponent } from './portal.component';
import { HomeComponent } from './home/home.component';
import { FaqComponent } from './faq/faq.component';
import { UserProfileComponent } from 'src/app/portal/user-profile/user-profile.component';
import { UserNotificationsComponent } from 'src/app/portal/user-notifications/user-notifications.component';
import { ReportTableComponent } from 'src/app/portal/report-table/report-table.component';
import { ReportListComponent } from 'src/app/portal/report-list/report-list.component';

const routes: Routes = [
  {
    path: '', component: PortalComponent, //sets us up to use an admin layout for all child components, come back to this
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'faq', component: FaqComponent },
      { path: 'my-profile', component: UserProfileComponent },
      { path: 'my-messages', component: UserNotificationsComponent },
      { path: 'report/list', component: ReportListComponent },
      { path: 'report/:id', component: ReportTableComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)], //, { useHash: true })],
  exports: [RouterModule]
})
export class PortalRoutingModule { }
