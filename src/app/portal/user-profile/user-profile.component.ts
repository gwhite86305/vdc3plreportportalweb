import { Component, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { UserService } from 'src/app/shared/services/user.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { UserModel, UserNew } from 'src/app/portal/models/users';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';

var CLASS_NAME: string = "UserProfileComponent";

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})

@Injectable()
export class UserProfileComponent {
  frmEdit: FormGroup;
  item: UserModel = JSON.parse(JSON.stringify(UserNew));
  submitted = false;
  error = '';

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private svc: UserService,
    private svcUtility: UtilityService,
    private svcAuthenticate: AuthenticationService
  ) {
  }

  // convenience getter for easy access to form fields
  get f() { return this.frmEdit.controls; }

  ngOnInit() {
    console.log('ngOnint');
    this.initFormGroup(); //call this early w/ empty model for proper initialization
    this.loadData();
  }

  //in this scenario, we are getting current user 
  loadData() {
    this.svcAuthenticate.currentUser.subscribe(data => {
      //assign the value to the user object used in the display
      this.item = data;
      this.initFormGroup(); //call this early w/ empty model for proper initialization
    },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
    });

  }

  initFormGroup() {
    this.frmEdit = this.formBuilder.group({
  //     userName: [this.item.userName, Validators.required]
       firstName: [this.item.firstName, Validators.required]
      ,lastName: [this.item.lastName, Validators.required]
    });

  }

  onSubmit() {
    this.svcUtility.logMessage('onSubmit', CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    this.error = '';
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmEdit.invalid) {
      return;
    }

    //apply vals back to the model
 //   this.item.userName = this.frmEdit.controls.userName.value;
    this.item.firstName = this.frmEdit.controls.firstName.value;
    this.item.lastName = this.frmEdit.controls.lastName.value;

    //call service save which also updates currentUser object
    this.svcAuthenticate.updateCurrentUser(this.item)
      .pipe(first())
      .subscribe(
      data => {
        if (data) {
          this.svcUtility.displayMessage("Your profile has been saved.", this.svcUtility.LogMessageSeverity.Info);
        }
        else {
          this.svcUtility.displayMessage("An error occurred saving your profile.", this.svcUtility.LogMessageSeverity.Error);
        }
      },
      error => {
        this.svcUtility.displayMessage(this.svcUtility.translateHTTPError(error), this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
      });
  }
}
