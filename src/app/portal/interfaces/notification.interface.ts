export interface NotificationModel {
  id: any;
  name: string;
  description: string; 
  category: string; 
  severity: string; 
  dateCreated: Date;
  dateActive: Date;
  isActive: boolean; 
}

