import { Component, Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/internal/Observable';
import { Subscription } from 'rxjs/internal/Subscription';
import { first, takeUntil } from 'rxjs/operators';
import 'rxjs/add/operator/takeUntil';

import { NotificationModel } from 'src/app/portal/interfaces/notification.interface';
import { NotificationService } from 'src/app/portal/services/notification.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UserModel } from 'src/app/portal/models/users';

var CLASS_NAME: string = "NotificationListComponent";

@Component({
  selector: 'notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
  
@Injectable()
export class NotificationListComponent implements OnInit {

  subUser: Subscription;
  dataRows: Array<NotificationModel> = [];

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(
    private svc: NotificationService,
    private svcUtility: UtilityService,
    private svcAuthenticate: AuthenticationService,
  ) {
  }

  ngOnInit() {
    this.svcUtility.logMessage("ngOnInit", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    var isLoaded: Subject < boolean > = new Subject<boolean>();
    //get user id once available, then load the data
    this.subUser = this.svcAuthenticate.currentUser.pipe(takeUntil(isLoaded))
      .subscribe(data => {
        if (data != null) {
          this.loadData(data.id);
          //unsubscribe to the observable once we get the userId value. It may not be there yet on initial login.
          //this.subUser.unsubscribe();
          isLoaded.next(true);
        }
      });
/*
    this.subUser..pipe(takeUntil(isLoaded))
    .pipe(
      subscribe(data => {
        if (data != null) {
          this.loadData(data.id);
          //unsubscribe to the observable once we get the userId value. It may not be there yet on initial login.
          //this.subUser.unsubscribe();
          isLoaded.next(true);
        }
      ));
      //.takeUntil(isLoaded)
//      }).takeUntil(isLoaded); //.unsubscribe();
*/
  }

  loadData(userId: any) {
    var userId: any;

    this.svc.getItems_ByUserID(userId)
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data;
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
      });
  }

}
