import { TestBed, async, tick, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NotificationListComponent } from 'src/app/portal/notification-list/notification-list.component';
import { NotificationService } from 'src/app/portal/services/notification.service';
import { UtilityService } from 'src/app/shared/services/utility.service';


var _startTime:any;
var CLASS_NAME: string = "NotificationListComponent";

describe(CLASS_NAME, () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule
      ],
      declarations: [
        NotificationListComponent
      ],
      providers: [NotificationService, UtilityService]
    }).compileComponents();
  }));

  it(': should create the component', () => {
    _startTime = new Date();
    (new UtilityService()).logMessage('create component', CLASS_NAME);
    const fixture = TestBed.createComponent(NotificationListComponent);
    const item = fixture.debugElement.componentInstance;
    expect(item).toBeTruthy();
  });

  //run an async test which waits for data to be loaded before performing tests in whenstable
  it(': finished rendering', async () => {
    const fixture = TestBed.createComponent(NotificationListComponent);
    const item = fixture.componentInstance;
    fixture.whenRenderingDone().then(() => {
      (new UtilityService()).logMessage_ElapsedTime('Finished Rendering', CLASS_NAME, _startTime, new Date());
      //TBD - test the HTML for existence of an expected number of rows
      expect(0).toBeLessThan(1);
    });
  });

  //run an async test which waits for data to be loaded before performing tests in whenstable
  it(': should load data rows', async () => {
    const fixture = TestBed.createComponent(NotificationListComponent);
    const item = fixture.componentInstance;
    item.ngOnInit();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(fixture.componentInstance.dataRows.length).toBeGreaterThan(0);
    });
  });

  //run an async test which waits for data to be loaded before performing tests in whenstable
  it(': load duration length', async () => {
    const fixture = TestBed.createComponent(NotificationListComponent);
    const item = fixture.componentInstance;
    item.ngOnInit();
    fixture.whenStable().then(() => {
      //log long the load data took, 
      (new UtilityService()).logMessage_ElapsedTime('ngOnInit, Load Data', CLASS_NAME, _startTime, new Date());
      expect(item.dataRows.length).toBeGreaterThan(0); //, "Elasped Time: " + elapsedTime);   // to be under 1 second
    });
  });

  //run an async test which waits for data to be loaded before performing tests in whenstable
  it(': looks async but is synchronous', <any>fakeAsync((): void => {
    let flag = false;
    setTimeout(() => { flag = true; }, 100);
    expect(flag).toBe(false);
    tick(50);
    expect(flag).toBe(false);
    tick(50);
    expect(flag).toBe(true);
  }));

});
