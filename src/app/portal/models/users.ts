import { PartnerModel } from "src/app/admin/interfaces/partner.interface";

export class Role {
  id: any;
  name: string;
}
/* already implemented in src\app\admin\interfaces\partner.interface.ts
export class Partner {
  id: number;
  tplPartnerID: number;
  name: string;
  description: string;
  dateCreated: string;
  dateUpdated: string;
  active: boolean;
}
*/

export class Permission {
  id: any;
  name: string;
  description: string;
  assetID: number;
}

export class UserModel {
  id: number;
  username: string; 
  email: string;
  firstName: string;
  lastName: string;
  partner: PartnerModel;
  roles: Array<Role>;
  permissions: Array<Permission>;
  active: boolean;  
  locked: boolean;  
}

//new instance of a user
export const UserNew: UserModel = {
  id: null,
  username: '',
  email: '',
  firstName: '',
  lastName: '',
  partner: null,
  roles: [],
  permissions: [],
  active: true, 
  locked: false 
};





