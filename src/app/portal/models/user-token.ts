export class UserToken{
  userId: any;
  token: string;
  expirationDate: Date;
}
