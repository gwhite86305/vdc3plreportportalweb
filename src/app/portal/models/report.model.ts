export class ReportModel {
  id: number;
  dateUpdated: Date;
  description: string;
  isSourceManual: boolean;
  name: string;
  totalRowCount: number;
  width: any;
}

export class ReportMetaDataModel extends ReportModel {
  reportColumns: Array<ReportColumnMetaDataModel>;
  filters: Array<ReportFilterModel>;
}

export class ReportDataModel extends ReportMetaDataModel {
  data: Array<any>;
}

export class ReportColumnMetaDataModel {
  id: number;
  name: string;
  description: string;
  alignment: string;
  formatString: string;
  dataType: string;
  sortable: true;
  cssClass:string;
}

export class ReportFilterModel {
  fieldName: string;
  description: string;
  value: string;
  criteria: string;  //make this an enum >, <, ==, contains, starts with, ends with
}

export class ReportSortModel {
  colName: string;
  isDescending: boolean = false; 
}
