import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { UserModel, Role, Permission } from 'src/app/portal/models/users';
import { UserToken } from 'src/app/portal/models/user-token';
import { GlobalConstants, AppPermissionEnum} from 'src/app/shared/services/global-settings.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

var CLASS_NAME: string = 'AuthenticationService';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  //TBD - come back to this and add the interceptors to add proper stuff to headers on requests and then
  // to intercept 401 responses and log user out. Also check out Mock back end provider interceptor,
  // Finally look at auth guard stuff related to this. 
  //http://jasonwatmore.com/post/2018/09/07/angular-6-basic-http-authentication-tutorial-example

  constructor(private http: HttpClient, private router: Router, private svcUtility: UtilityService) { }

  ///--------------------------------------------------------------------------
  /// These observables are in use so subscribers will be notified as values change
  ///--------------------------------------------------------------------------
  currentUser: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(this.getCurrentUser());
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.getIsAuthenticated());
  
  ///--------------------------------------------------------------------------
  /// Login the user and then call the initUser to load user data
  ///--------------------------------------------------------------------------
  login(username: string, password: string) {
    //TBD - get this working where post vars can be passed in as param body
    //return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/Login', { username: username, password: password })
    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/Login?username=' + username + '&password=' + password, { username: username, password: password })
            .pipe(map((res:any) => {
                // login successful if there's a jwt token in the response
                if (res && res.token) {
                  // store username and jwt token in local storage to keep user logged in between page refreshes
                  var tokenData: UserToken = { userId: res.userID, token: res.token, expirationDate: res.expirationDate };
                  //expiresAt: new Date((new Date()).getTime() + GlobalConstants.SessionLength)
                  localStorage.setItem('userToken', JSON.stringify(tokenData));
                  //update the subscription to indicate value is now present
                  this.isAuthenticated.next(true);

                  //initialize the current user
                  this.initializeCurrentUser(tokenData)
                    .pipe(first())
                    .subscribe(
                    data => {},
                    error => {
                      //TBD - may need to throw this error as well
                      this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
                    });
                }
            }));
    }

    ///--------------------------------------------------------------------------
    /// Check is user token exists and is not expired. 
    /// This should pull the token from its sesison location so that the calling code should not
    /// be aware of the token
    ///--------------------------------------------------------------------------
    private getIsAuthenticated() {
      // check if user is authenticated
      var result = this.tokenData;
      if (result == null) return false;
      //TBD - beef this up. Check for an expiry datetime
      if (new Date(result.expirationDate).getTime() < (new Date()).getTime())
      {
          //this.logout(); //called from app.component which listens for changes to isAuthenticated observable
          return false;
      }
      //if we get here, all good
      return true;
    }

  ///--------------------------------------------------------------------------
  /// Get access token that was assigned at login
  /// return entire object so calling code can check expiry, etc. 
  ///--------------------------------------------------------------------------
  get tokenData() {
      var tokenVal: string = localStorage.getItem('userToken');
      if (tokenVal == null) return null;
      var result: UserToken = JSON.parse(tokenVal);
      return result;
    }

  ///--------------------------------------------------------------------------
  /// Extend Access token on each request
  ///--------------------------------------------------------------------------
  extendTokenExpiry() {
    //TODO: call API to extend token expiration date
    var tokenData: UserToken = this.tokenData;
    tokenData.expirationDate = new Date((new Date()).getTime() + GlobalConstants.SessionLength);
    localStorage.setItem('userToken', JSON.stringify(tokenData));
  }

  logout() {
    // remove user from local storage to log user out
    //change the observer values so subscribers know and will trigger appropriate action. 
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userToken');
    this.currentUser.next(null);
    this.svcUtility.setSystemProcessing(false);
    //app.component is subscribing to this and will navigate to login
    this.isAuthenticated.next(false);  
  }

  ///--------------------------------------------------------------------------
  /// call the API to get the current user and save it locally to storage
  /// a separate observable will allow subscribers to listed for when this changes. 
  ///--------------------------------------------------------------------------
  private initializeCurrentUser(tokenData: UserToken) {
    //call API, then set value in local storage
    //return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUserByID', { userID: tokenData.userId})
    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUserByID?userID=' + tokenData.userId, { userID: tokenData.userId })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
        // this.getIsAuthenticated: UserModel = res;
          localStorage.setItem('currentUser', JSON.stringify(res));
          //update the subscription to indicate value is now present
          
          var selectedRole = this.getselectedRole(res);
                      
          if (this.isEmptyObject(selectedRole) ) {
             console.log('No Role found');
          } else {   
             localStorage.setItem('role', JSON.stringify(selectedRole));
          }
          
          var selectedPermissions = res.permissions;

          if (this.isEmptyObject(selectedPermissions) ) {
            console.log('No Permissions found');
          } else {   
            localStorage.setItem('permissions', JSON.stringify(selectedPermissions));
          }

          this.currentUser.next(res);
        }
      }));
  }

//Given a User's id return its role object
  getselectedRole(res: UserModel ) {
    let id = res.id
    let selectedRole: Role = res.roles.find(i=> i.id == id);

    if (this.isEmptyObject(selectedRole)) {
      console.log('No role found for id: ", id');
    } else {
       console.log('role returned is:', selectedRole);
       return selectedRole;
    }
  }

  getselectedPermission(res: UserModel) {
    let assetID = 200;
    let selectedpermissions: Permission = res.permissions.find(i => i.assetID == assetID); 

    if (this.isEmptyObject(selectedpermissions)) {
       console.log('No permissions found');
    } else {
       localStorage.setItem('permissions', JSON.stringify(selectedpermissions));
       console.log('Permissions: AssetID:', selectedpermissions.assetID);
       console.log('Permissions: Name:', selectedpermissions.name);
       console.log('Permissions: Description', selectedpermissions.description);
    }
    return selectedpermissions;
  }

  isEmptyObject(obj) {
    for (var prop in obj) {
       if (obj.hasOwnProperty(prop)) {
          return false;
       }
    }
  }

  ///--------------------------------------------------------------------------
  /// Get the currently logged in user object
  /// If user object is null, then call the API
  /// This should pull the token from its sesison location so that the calling code should not
  /// be aware of the token
  ///--------------------------------------------------------------------------
  private getCurrentUser(): UserModel {
    //if user is not authenticated. return null
    if (!this.isAuthenticated) return null;

    //get from local storage
    if (localStorage.getItem('currentUser') != null) {
      var result: UserModel = JSON.parse(localStorage.getItem('currentUser'));
      return result;
    }
    return null;
  }

  ///--------------------------------------------------------------------------
  /// Get the currently logged in user permissions
  ///--------------------------------------------------------------------------
  hasPermissionCurrentUser(permissionName: AppPermissionEnum): boolean{
    //if user is not authenticated. return null
    var user: UserModel = this.getCurrentUser();
    if (user == null) return false;

    return this.hasPermission(user.permissions, permissionName);
  }

  ///--------------------------------------------------------------------------
  /// check if user has permission
  ///--------------------------------------------------------------------------
  private hasPermission(permissions: Array<Permission>, permissionName: AppPermissionEnum): boolean {
    if (permissions == null || permissions.length == 0) return false;

    var result: boolean = permissions.some((item: Permission) => {
      return (item.name.toLowerCase() == permissionName.toLowerCase());
    });

    return result;
  }

  ///--------------------------------------------------------------------------
  /// Update user profile - change to new endpoint once update profile avail.
  ///--------------------------------------------------------------------------
  updateCurrentUser(item: UserModel): Observable<boolean> {

    const url: string = GlobalConstants.BASE_API_URL + 'user/update';
    return this.http.post<any>(url, item, { responseType: 'text' as 'json' } )
      .pipe(map((res: any) => {
        var result: any = null;
        if (res) {
          localStorage.setItem('currentUser', JSON.stringify(item));
          this.currentUser.next(item);
          return true;
        }
        return false;
      }));
  }

}
