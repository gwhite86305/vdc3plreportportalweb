import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';
import { FaqModel } from 'src/app/portal/interfaces/faq.interface';

const CLASS_NAME: string = 'FaqService'; //used for logging to console

///--------------------------------------------------------------------------
/// FaqService - Class which interacts with web API to get FAQ data
///--------------------------------------------------------------------------
@Injectable()
export class FaqService {

    ///--------------------------------------------------------------------------
    /// C'tor
  ///--------------------------------------------------------------------------
  constructor(private http: HttpClient, private svcUtility: UtilityService) { }

  ///--------------------------------------------------------------------------
  /// Get All
  ///--------------------------------------------------------------------------
  getItems_All(): Observable<Array<FaqModel>> {

    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'Faq/GetAll', {})
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return user object
          var result: Array<FaqModel> = res;
          return result;
        }
      }));
  }

}
