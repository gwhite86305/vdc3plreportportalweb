import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { GlobalConstants, ReportDownloadFormat } from 'src/app/shared/services/global-settings.service';
import { NotificationModel } from 'src/app/portal/interfaces/notification.interface';
import { ReportMetaDataModel, ReportDataModel, ReportFilterModel, ReportSortModel, ReportModel } from 'src/app/portal/models/report.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ReportData_Mock } from 'src/app/portal/services/mock.data';

const CLASS_NAME: string = 'ReportService'; //used for logging to console

///--------------------------------------------------------------------------
/// Service - Class which interacts with web API to get report data, report meta data
///--------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class ReportService {

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(private http: HttpClient, private svcUtility: UtilityService) { }

  ///--------------------------------------------------------------------------
  /// Get report list data
  ///--------------------------------------------------------------------------
  getItems_All(): Observable<Array<ReportModel>> {

    //var useMock: boolean = true;
    //if (useMock) return this.getItems_All_Mock();

    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'report/getall')
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //the expectation is the structure will be report info, report metadata (high level), no data, no report columns metadata
          var result: Array<ReportMetaDataModel> = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Get report data
  ///--------------------------------------------------------------------------
  getItems_ByReportId(reportId: any,
    includeMetaData: boolean = true, startIndex: number = 1, length: number = GlobalConstants.PageSize,
    sort: Array<ReportSortModel> = null, filter: Array<ReportFilterModel> = null):
    Observable<ReportDataModel> {

    //TODO: Add default date range filter
    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'report/getall')
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //the expectation is the structure will be report info, report metadata (high level), no data, no report columns metadata
          //TBD - temp - get all reports, filter on the report id, then call mock to get the mock for this id.
          //This will change to just call get by report id and only get report model, meta, data
          var items: Array<ReportMetaDataModel> = res;
          var result: any = null;
          items.some((item: ReportDataModel) => {
            if (item.id == reportId) result = item;
            return item.id == reportId;
          });
          return this.getItems_ByReportId_PartialMock(result, reportId, startIndex, length, sort);
        }
      }));

    /*
    var useMock: boolean = true;
    if (useMock) return this.getItems_ByReportIdMock(reportId, startIndex, length, sort);

    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'report/getById',
      {
        reportID: reportId, startIndex: startIndex, length: length,
        sort: sort, filter: filter
      })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //the expectation is the structure will be report.data containing rows of data and report.metadata (optional) containing metadata
          //combining both ino a single call so the caller can build out the column structure and display at same time. 
          var result: ReportDataModel = res;
          return result;
        }
      }));
      */
  }

  ///--------------------------------------------------------------------------
  /// download Report data
  ///--------------------------------------------------------------------------
  downloadItem_ByReportId(reportId: any,
    sort: Array<ReportSortModel> = null, filter: Array<ReportFilterModel> = null,
    format: ReportDownloadFormat = ReportDownloadFormat.CSV): Observable<any> {

    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'report/download',
      {
        reportID: reportId, sort: sort, filter: filter, format: format
      })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          return true; //this will eventually return something to stream to new window. 
        }
      }));
  }


  //call a mock data 
  private getItems_All_Mock() {
    var data: Array<ReportDataModel> = ReportData_Mock;
    var result: Array<ReportModel> = data.map(function (item: ReportDataModel) {
      var itemMap: ReportModel = item;
        //{
        //id: item.id, name: item.name, description: item.description, dateUpdated: item.dateUpdated,
        //totalRowCount: item.totalRowCount, isSourceManual: item.isSourceManual};
      return itemMap;
    });
    return new BehaviorSubject<Array<ReportModel>>(result);
  }

  //call a partial mock data, it takes the data from here but the metadata from the API
  private getItems_ByReportId_PartialMock(report: ReportDataModel, reportId: any, startIndex: number = 1, length: number = GlobalConstants.PageSize,
    sort: Array<ReportSortModel> = null) {

    //get mock data and append to report object
    var reports: Array<ReportDataModel> = JSON.parse(JSON.stringify(ReportData_Mock));
    reports.some((item: ReportDataModel) => {
      if (item.id == reportId) report.data = item.data;
      return item.id == reportId;
    });

    //sort the data
    var existingSort: ReportSortModel = sort == null || sort.length == 0 || !sort[0].isDescending ? new ReportSortModel() : <ReportSortModel>{ colName: sort[0].colName, isDescending: false };
    var sortResult = sort.length == 0 ? report.data : this.svcUtility.sortColumn(existingSort, report.data, sort[0].colName).data;
    //get total row count for pager
    report.totalRowCount = sortResult.length;
    //slice data to the page
    report.data = sortResult.slice(startIndex - 1, startIndex - 1 + length);
    //return the data
    return report;
  }

  //mock the get report by id call
  private getItems_ByReportIdMock(reportId: any, startIndex: number = 1, length: number = GlobalConstants.PageSize,
    sort: Array<ReportSortModel> = null) {

    var result: ReportDataModel = null;
    var reports: Array<ReportDataModel> = JSON.parse(JSON.stringify(ReportData_Mock));
    reports.some((item: ReportDataModel) => {
      if (item.id == reportId) result = item;
      return item.id == reportId;
    });

    //sort the data
    var existingSort: ReportSortModel = sort  == null || sort.length == 0 || !sort[0].isDescending ? new ReportSortModel() : <ReportSortModel>{ colName: sort[0].colName, isDescending: false };
    var sortResult = sort.length == 0 ? result.data : this.svcUtility.sortColumn(existingSort, result.data, sort[0].colName).data;
    //get total row count for pager
    result.totalRowCount = sortResult.length;
    //slice data to the page
    result.data = sortResult.slice(startIndex-1, startIndex - 1 + length);
    //return the data
    return new BehaviorSubject<ReportDataModel>(result);
  }

}
