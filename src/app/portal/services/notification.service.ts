import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';
import { NotificationModel } from 'src/app/portal/interfaces/notification.interface';

const CLASS_NAME: string = 'NotificationService'; //used for logging to console

///--------------------------------------------------------------------------
/// FaqService - Class which interacts with web API to get FAQ data
///--------------------------------------------------------------------------
@Injectable()
export class NotificationService {

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(private http: HttpClient, private svcUtility: UtilityService) { }

  ///--------------------------------------------------------------------------
  /// Get All
  ///--------------------------------------------------------------------------
  getItems_ByUserID(userId: any): Observable<Array<NotificationModel>> {

    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'notification/getbyuserid?userID=' + userId, { userID: userId })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return user object
          var result: Array<NotificationModel> = res;
          return result;
        }
      }));
  }

}
