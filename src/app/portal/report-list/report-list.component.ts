import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { first } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { ReportSortModel, ReportModel } from 'src/app/portal/models/report.model';
import { ReportService } from 'src/app/portal/services/report.service';
import { EntityInfoModel } from 'src/app/shared/interfaces/entityInfo.interface';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';

const CLASS_NAME: string = 'ReportListComponent'; //used for logging to console

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css']
})
export class ReportListComponent implements OnInit {
  entityInfo: EntityInfoModel = {
    name: 'Report',
    namePlural: 'Reports',
    url: '/portal/report'
  };

  dataRows: Array<ReportModel>;
  dataRowsFiltered: Array<ReportModel>;
  filterVal: string = '';
  sort: ReportSortModel = { colName: '', isDescending: false };

  constructor(
    private route: ActivatedRoute,
    private svc: ReportService,
    private svcUtility: UtilityService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  g = GlobalConstants;

  public loadData() {

    this.svcUtility.setSystemProcessing(true);
    this.svc.getItems_All()
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data;
        this.dataRowsFiltered = data;
        //initial sort
        this.sortColumn('name');
        this.svcUtility.setSystemProcessing(false);
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.setSystemProcessing(false);
      });
  }

  //on filter click
  onFilter(val) {
    this.svcUtility.logMessage('on Filter: ' + val, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    //get copy of original and then apply filter to it.
    var filtered: Array<ReportModel> = JSON.parse(JSON.stringify(this.dataRows));
    //if there is a value in filtered, perform the filtering
    if (val.length > 0) {
      // Filter data
      filtered = filtered.filter((item: ReportModel) => {
        var searchStr = (item.description.toLowerCase() + '|||' + item.name.toLowerCase());
        return searchStr.indexOf(val.toLowerCase()) !== -1;
      });
    }
    //update the filtered array
    this.dataRowsFiltered = filtered;
  }

  //sort
  sortColumn(colName: string) {
    this.svcUtility.logMessage('sortColumn:' + colName, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    var result = this.svcUtility.sortColumn(this.sort, this.dataRowsFiltered, colName);
    this.dataRowsFiltered = result.data;
    this.sort = result.sort;
  }

}

