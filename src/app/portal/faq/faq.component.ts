import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { FaqModel } from 'src/app/portal/interfaces/faq.interface';
import { FaqService } from 'src/app/portal/services/faq.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

var CLASS_NAME: string = "FaqComponent";

@Component({
  selector: 'faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})

@Injectable()
export class FaqComponent implements OnInit {

  dataRows: Array<FaqModel>= [];

  ///--------------------------------------------------------------------------
  /// C'tor
  ///--------------------------------------------------------------------------
  constructor(
    private svc: FaqService,
    private svcUtility: UtilityService
  ) {
    console.log(this.svcUtility.getlogMessageString("constructor", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info));
  }

  ngOnInit() {

    this.svc.getItems_All()
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data;
      },
      error => {
        //this.svcUtility.displayMessage(this.svcUtility.translateHTTPError(error), this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        //this.loading = false;
      });
  }
}
