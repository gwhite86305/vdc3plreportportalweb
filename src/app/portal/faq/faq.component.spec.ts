import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FaqComponent } from 'src/app/portal/faq/faq.component';
import { FaqService } from 'src/app/portal/services/faq.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

describe('FaqComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule
      ],
      declarations: [
        FaqComponent
      ],
      providers: [FaqService, UtilityService]
    }).compileComponents();
  }));

  it(': should create the component', () => {
    const fixture = TestBed.createComponent(FaqComponent);
    const item = fixture.debugElement.componentInstance;
    expect(item).toBeTruthy();
  });

  it(': should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(FaqComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent.length).toBeGreaterThan(0);
  });
});
