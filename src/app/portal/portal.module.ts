import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgDatepickerModule } from 'ng2-datepicker';
import { PortalRoutingModule } from './portal-routing.module';
import { PortalComponent } from './portal.component';
import { HomeComponent } from './home/home.component';
import { FaqComponent } from './faq/faq.component';
import { UserProfileComponent } from 'src/app/portal/user-profile/user-profile.component';
import { UserNotificationsComponent } from 'src/app/portal/user-notifications/user-notifications.component';
import { FaqService } from 'src/app/portal/services/faq.service';
import { NotificationService } from 'src/app/portal/services/notification.service';
import { NotificationListComponent } from 'src/app/portal/notification-list/notification-list.component';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { ReportListComponent } from 'src/app/portal/report-list/report-list.component';
import { ReportTableComponent } from 'src/app/portal/report-table/report-table.component';
import { PipeModule } from 'src/app/shared/pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgDatepickerModule,
    PortalRoutingModule,
    SharedModule,
    PipeModule
  ],
  declarations: [
      PortalComponent
    , HomeComponent
    , FaqComponent
    , UserProfileComponent
    , UserNotificationsComponent
    , NotificationListComponent
    , ReportListComponent
    , ReportTableComponent
  ],
  providers: [FaqService, NotificationService, UtilityService],
  bootstrap: [PortalComponent]
})
export class PortalModule { }
