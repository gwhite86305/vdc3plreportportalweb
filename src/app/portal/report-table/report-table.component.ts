import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { first } from 'rxjs/operators';

import { UtilityService } from 'src/app/shared/services/utility.service';
import { ReportService } from 'src/app/portal/services/report.service';
import { ReportSortModel, ReportFilterModel, ReportMetaDataModel } from 'src/app/portal/models/report.model';
import { GlobalConstants, ReportDownloadFormat } from 'src/app/shared/services/global-settings.service';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UserModel } from 'src/app/portal/models/users';

import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';

const CLASS_NAME: string = 'ReportTableComponent'; //used for logging to console

@Component({
  selector: 'app-report-table',
  templateUrl: './report-table.component.html',
  styleUrls: ['./report-table.component.css']
})
export class ReportTableComponent implements OnInit {

  //column metadata, report filters
  report: ReportMetaDataModel = {
    id: 0, totalRowCount: 0, name: '', description: 'Report', dateUpdated: null, isSourceManual: false, width: null, reportColumns: null, filters: null  };
  startIndex: number = 1;
  pageSize: number = GlobalConstants.ReportPageSize;
  pageSizeOptions: Array<number> = GlobalConstants.PageSizeOptions;
  pager: Array<number> = [];
  currentPage: number = 1;
  filterArray: Array<ReportFilterModel> = []; 
  sortArray: Array<ReportSortModel> = [];

  //report data
  dataRows: Array<any>;
  dataRowsFiltered: Array<any>;

  //used to coordinate scroll
  @ViewChild('reportHeader') reportHeader;
  @ViewChild('reportBody') reportBody;
  @ViewChild('reportData') reportData;

  //filters
  filterToggle: boolean = false;
  filterNumberOperators: any = [{ name: "num_lt", caption: "Less than" }, { name: "num_lte", caption: "Less than/equal" }, { name: "num_equal", caption: "equal" }, { name: "num_gte", caption: "Greater than/equal" }, { name: "num_gt", caption: "Greater than or equal" }];
  filterDateOperators: any = [{ name: "dt_lt", caption: "<" }, { name: "dt_lte", caption: "<=" }, { name: "dt_equal", caption: "=" }, { name: "dt_gte", caption: ">=" }, { name: "dt_gt", caption: ">" }];

  //datepicker options
  dtOptions: DatepickerOptions = Object.assign({}, GlobalConstants.DatePickerOptions);

  constructor(
    private route: ActivatedRoute,
    private svc: ReportService,
    private svcUtility: UtilityService,
    private svcAuthenticate: AuthenticationService
  ) { }

  // convenience getter for easy access to form fields
  get g() { return GlobalConstants; }


  ngOnInit() {
    console.log('hello');
    this.report.id = this.route.snapshot.params["id"];
    this.loadData(this.report.id, true);
  }

  public loadData(reportId: any, includeMetadata: boolean) {

    this.svcUtility.setSystemProcessing(true);
    this.svc.getItems_ByReportId(reportId, includeMetadata, this.startIndex, this.pageSize,
      this.sortArray, this.filterArray)
      .pipe(first())
      .subscribe(
      data => {
        this.dataRows = data.data;
        this.dataRowsFiltered = data.data;
        this.report.id = data.id;
        this.report.totalRowCount = data.totalRowCount;
        this.report.description = data.description;
        this.report.name = data.name;
        this.report.dateUpdated = data.dateUpdated;
        this.report.isSourceManual = data.isSourceManual;
        this.report.reportColumns = data.reportColumns;
        this.report.filters = data.filters;
        this.pager = this.svcUtility.initGridPager(data.totalRowCount, this.pageSize);
        this.svcUtility.setSystemProcessing(false);
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.displayMessage("An error occurred retrieving your report. Please try again.", this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.setSystemProcessing(false);
      });
  }

  //set the sort indicator based on potential of multi-column sort
  setSortIndicator(colName: string) {
    //set sort indicator based on current sort condition.
    var itemSort: ReportSortModel = null;
    this.sortArray.some((item: ReportSortModel) => {
      if (item.colName == colName) itemSort = item;
      return item.colName == colName;
    });

    if (itemSort && itemSort.colName == colName) {
      return itemSort.isDescending ? "fa fa-sort-down" : "fa fa-sort-up";
    }
    return "none";
  }

  //sort - build new sort order array and then call load data to re-load data from
  // aPI with different sort
  sortColumn(colName: string) {
    this.svcUtility.logMessage('sortColumn:' + colName, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    var itemSort: ReportSortModel = null;
    this.sortArray.some((item: ReportSortModel) => {
      if (item.colName == colName) itemSort = item;
      return item.colName == colName;
    });
    var newSort: Array<ReportSortModel> = [];
    newSort.push({ colName: colName, isDescending: itemSort != null ? !itemSort.isDescending : false });
    this.sortArray = newSort;
    this.startIndex = 1;
    this.currentPage = 1;
    this.loadData(this.report.id, true);
  }

  //Page the grid data
  pageData(pageNum: number) {
    if (pageNum == this.currentPage) return;
    this.svcUtility.logMessage('pageData:' + pageNum, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    this.startIndex = this.pageSize * (pageNum-1) + 1;
    this.currentPage = pageNum;
    this.loadData(this.report.id, true);
    //scroll screen to top
    window.scrollTo({ top: (this.reportHeader.nativeElement.offsetTop), behavior: 'smooth' });
  }

  //Page the grid data
  changePageSize() {
    //note model binding in html automatically updating the pageSize value. 
    this.svcUtility.logMessage('changePageSize:' + this.pageSize, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    this.startIndex = 1;
    this.currentPage = 1;
    this.loadData(this.report.id, true);
    //scroll screen to top
    window.scrollTo({ top: (this.reportHeader.nativeElement.offsetTop), behavior: 'smooth' });
  }

  //synch scrolling of a virtual scroll with actual scroll
  //onScrollVirtual($event) {
  //  //console.log($event);
  //  //this.reportBody.nativeElement.scrollTo({ left: (this.reportHeader.nativeElement.scrollLeft), behavior: 'smooth' });
  //}
  //onScroll($event) {
  //  //console.log($event);
  //}
  scroll(val:number) {
    console.log('scroll');
    var valDestination: number = this.reportHeader.nativeElement.scrollLeft + val;
    this.reportHeader.nativeElement.scrollTo({ left: (valDestination), behavior: 'smooth' });
    this.reportBody.nativeElement.scrollTo({ left: (valDestination), behavior: 'smooth' });
  }
  //TODO: add check to see if report body is wider than report-container
  isScrollVisible() {
    //before report is init, this will not be present
    if (this.reportData == undefined || this.reportBody == undefined) return false;

    return (this.dataRowsFiltered != null && this.dataRowsFiltered.length > 0
      && this.report.width != null &&
      this.reportData.nativeElement.offsetWidth > this.reportBody.nativeElement.offsetWidth-100); 
  }

  //download PDF, CSV using current filters, sorts, etc.
  downloadPDF() {
    this.downloadReport(ReportDownloadFormat.PDF);
  }
  downloadCSV() {
    this.downloadReport(ReportDownloadFormat.CSV);
  }

  //download PDF, CSV using current filters, sorts, etc.
  downloadReport(format: ReportDownloadFormat) {
    //note model binding in html automatically updating the pageSize value. 
    this.svcUtility.logMessage('downloadReport:' + format, CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
    this.svc.downloadItem_ByReportId(this.report.id,
      this.sortArray, this.filterArray, format)
      .pipe(first())
      .subscribe(
      data => {
        console.log('TBD');
      },
      error => {
        this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
        this.svcUtility.displayMessage("An error occurred downloading your report. Please try again.", this.svcUtility.LogMessageSeverity.Error);
      });
  }

  //on filter click
  onFilter() {
    console.log('filter');
    this.svcUtility.logMessage('on Filter: ' + JSON.stringify(this.report.filters), CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
/*
    //get copy of original and then apply filter to it.
    var filtered: Array<UserModel> = JSON.parse(JSON.stringify(this.dataRows));
    //if there is a value in filtered, perform the filtering
    if (val.length > 0) {
      // Filter data
      filtered = filtered.filter((client: UserModel) => {
        var searchStr = (client.firstName.toLowerCase() + '|||' + client.lastName).toLowerCase();
        return searchStr.indexOf(val.toLowerCase()) !== -1;
      });
    }
    //update the filtered array
    this.dataRowsFiltered = filtered;
*/
  }

  filterClass(col: ReportFilterModel) {
    if (col.criteria.indexOf('dt_') > -1) return "datetime";
    if (col.criteria.indexOf('num_') > -1) return "number";
    return "";
  }

  toggleMyFilter() {
    this.filterToggle = !this.filterToggle;
  }
}

