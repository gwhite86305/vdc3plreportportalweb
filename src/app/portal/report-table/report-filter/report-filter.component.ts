import { Component, Input, OnInit} from '@angular/core';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { ReportFilterModel} from 'src/app/portal/models/report.model';
import { GlobalConstants} from 'src/app/shared/services/global-settings.service';

const CLASS_NAME: string = 'ReportFilterComponent'; //used for logging to console

@Component({
  selector: 'report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.css']
})
export class ReportFilterComponent implements OnInit {

  @Input() filter: ReportFilterModel; 

  constructor(
    private svcUtility: UtilityService ) { }

  ngOnInit() {}

}

