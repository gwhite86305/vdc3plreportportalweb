import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
/*3rd Party packages*/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

/*App specific components*/
import { AppComponent } from './app.component';
import { LoginComponent } from './public/login/login.component';

import { EditClientDialogComponent} from '../app/admin/dialogs/edit/client-list/editClientList.dialog.component';

import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestInterceptor } from 'src/app/shared/services/httpRequest.interceptor';
import { PipeModule } from 'src/app/shared/pipes/pipe.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
    , LoginComponent
    , PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    AngularFontAwesomeModule,
    NgDatepickerModule,
    PipeModule,
    SharedModule
  ],
  providers: [UtilityService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
