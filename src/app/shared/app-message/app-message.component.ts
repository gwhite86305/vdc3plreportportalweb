import { Component, Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { DisplayMessageService } from 'src/app/shared/services/display-message.service';
import { DisplayMessagenModel } from 'src/app/shared/interfaces/displayMessage.interface';
import { LoggerSeverity } from 'src/app/shared/interfaces/logMessage.interface';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-message',
  templateUrl: './app-message.component.html',
  styleUrls: ['./app-message.component.scss']
})

@Injectable()
export class AppMessageComponent implements OnInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  errorMessages: Array<DisplayMessagenModel> = [];
  warnMessages: Array<DisplayMessagenModel> = [];
  infoMessages: Array<DisplayMessagenModel> = [];

  constructor(
    private svcDisplayMessages: DisplayMessageService ) { }

  ngOnInit() {
    this.svcDisplayMessages.currentMessages.subscribe(data => {
      //assign the value to the object used in the display
      this.errorMessages = this.filterMessages(data, LoggerSeverity.Error);
      this.warnMessages = this.filterMessages(data, LoggerSeverity.Warn);
      this.infoMessages = this.filterMessages(data, LoggerSeverity.Info);
    });
  }

  ngOnDestroy() {
    console.log('on destroy');
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  clearErrorMessages() {
    this.svcDisplayMessages.clearDisplayMessages(LoggerSeverity.Error);
  }
  clearWarnMessages() {
    this.svcDisplayMessages.clearDisplayMessages(LoggerSeverity.Warn);
  }
  clearInfoMessages() {
    this.svcDisplayMessages.clearDisplayMessages(LoggerSeverity.Info);
  }

  filterMessages(data: Array<DisplayMessagenModel>, severity: LoggerSeverity): Array<DisplayMessagenModel> {
    if (data == null || data.length == 0) return [];
    var result: Array<DisplayMessagenModel> = null;
    result = data.filter(function (msg: DisplayMessagenModel) {
      return msg.Severity == severity;
    });
    return result;
  }

}
