import { Injectable} from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { UserModel, Permission } from 'src/app/portal/models/users';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';

var CLASS_NAME: string = "CanManagePartnersGuard";

@Injectable({ providedIn: 'root' })
export class CanManagePartnersGuard implements CanActivate {

  constructor(private router: Router, private svcAuthenticate: AuthenticationService, private svcUtility: UtilityService) { }
  ngOnInit() {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(this.svcUtility.getlogMessageString("constructor", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info));
    var result: boolean = this.svcAuthenticate.hasPermissionCurrentUser(GlobalConstants.AppPermissionsEnum.CanManagePartners);
    // if not allowed redirect to portal home page 
    if (!result) {
      this.svcUtility.displayMessage("You are not permitted to enter this area. You have been redirected to the portal home page.", this.svcUtility.LogMessageSeverity.Error);
      this.router.navigate(['/portal/home']);
    }
    return result;
  }
}
