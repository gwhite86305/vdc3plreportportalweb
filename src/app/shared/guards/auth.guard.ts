import { Injectable} from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

var CLASS_NAME: string = "AuthGuard";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private svcAuthenticate: AuthenticationService, private svcUtility: UtilityService) { }
  ngOnInit() {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(this.svcUtility.getlogMessageString("constructor", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info));
    var isAuthenticated = false;
    this.svcAuthenticate.isAuthenticated.subscribe(isAuth => {
      isAuthenticated = isAuth;
    }).unsubscribe();

    if (isAuthenticated) return true;

      // not logged in so redirect to login page with the return url
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
      return false;
  }
}
