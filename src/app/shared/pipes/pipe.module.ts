import { NgModule } from '@angular/core';
import { ReportFormatPipe } from 'src/app/shared/pipes/report-format.pipe';

@NgModule({
  imports: [],
  declarations: [ReportFormatPipe],
  exports: [ReportFormatPipe]
})
export class PipeModule { }
