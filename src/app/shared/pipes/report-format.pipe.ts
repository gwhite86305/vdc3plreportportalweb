import { Pipe, PipeTransform } from '@angular/core';
import { formatDate, formatCurrency, formatNumber, formatPercent } from '@angular/common';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';

const CLASS_NAME: string = 'ReportFormatPipe'; 

///--------------------------------------------------------------------------
/// This will handle formatting of data values in a grid. 
///--------------------------------------------------------------------------
@Pipe({ name: 'reportFormat' })
export class ReportFormatPipe implements PipeTransform {

  transform(val: any, dataType?: string, format?: string): string {

    //console.log('reportFormat Pipe: value:' + val + ', dataType: ' + dataType + ', format: ' + format);

    if (typeof val === 'undefined' || val == null) { return ''; }
    if (typeof dataType === 'undefined' || dataType == null) { return val; }

    var result = val;

    switch (dataType.toLowerCase())
    {
      case 'currency':
        result = formatCurrency(val, 'en-us', '$', format != '' ? format : '1.2');
        break;
      case 'date':
        result = formatDate(val, format != '' ? format : GlobalConstants.DateFormat_Grid, 'en-us');
        break;
      case 'number':
        result = formatNumber(val, 'en-us', format != '' ? format : '1.2-5');
        break;
      case 'integer':
        result = parseInt(val);
        break;
      case 'percent':
        result = formatPercent(val, 'en-us', format != '' ? format : '');
        break;
      case 'string':
      default:
        return val;
    }
    return result;
  }

}
