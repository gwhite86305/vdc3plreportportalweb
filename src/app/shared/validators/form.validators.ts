import { AbstractControl } from '@angular/forms';

export function phoneNumberValidator(control: AbstractControl): { [key: string]: any } | null {
  const valid = /^\d+$/.test(control.value);
  return valid ? null : { invalidNumber: { valid: false, value: control.value } };
}

//check is the select is -1 value, empty or null
export function selectValidator(control: AbstractControl): { [key: string]: any } | null {
  console.log('select validator');
  const invalid = control.value == null || control.value == '' || (!isNaN(control.value) && parseInt(control.value) < 1 );
  return !invalid ? null : { invalidSelect: { valid: false, value: control.value } };
}
