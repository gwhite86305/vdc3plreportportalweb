///--------------------------------------------------------------------------
/// entityInfo model - This is used by certain components to help paramterize settings so
///   that we can accelerate re-use of similar components
///--------------------------------------------------------------------------
export interface EntityInfoModel {
    name: string,
    namePlural: string, 
    url: string
}

