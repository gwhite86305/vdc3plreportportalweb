import { LoggerSeverity } from "src/app/shared/interfaces/logMessage.interface";

///--------------------------------------------------------------------------
/// Display Message Model
///--------------------------------------------------------------------------
export interface DisplayMessagenModel {
  Message: string; 
  Severity: LoggerSeverity; 
  DateCreated: Date;
}

