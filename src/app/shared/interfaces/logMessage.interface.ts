///--------------------------------------------------------------------------
/// Log severity levels
///--------------------------------------------------------------------------
export enum LoggerSeverity {
  Info = 'Info'
  , Warn = 'Warn'
  , Error = 'Error'
  , Debug = 'Debug'
}

///--------------------------------------------------------------------------
/// Log Message Model
///--------------------------------------------------------------------------
export interface LogMessagenModel {
  Code: string;
  Message: string; 
  Category: string;
  Severity: LoggerSeverity; 
  DateCreated: Date;
}

///--------------------------------------------------------------------------
/// New Log Message
///--------------------------------------------------------------------------
export var LogMessageNew: LogMessagenModel = { Code: '', Message: '', Category: '', Severity: LoggerSeverity.Info, DateCreated: new Date() };

