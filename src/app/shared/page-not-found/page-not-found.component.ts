import { Component, Injectable, OnInit } from '@angular/core';
import { UserModel } from 'src/app/portal/models/users';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})

@Injectable()
export class PageNotFoundComponent implements OnInit {
  isAuthenticated: boolean = false;

  constructor(
    private svcAuthenticate: AuthenticationService,
    private svcUtility: UtilityService
  ) { }

  ngOnInit() {
    //this.isAuthenticated = this.svcAuthenticate.isAuthenticated;
    this.svcAuthenticate.isAuthenticated.subscribe(data => {
      //assign the value to the user object used in the display
      this.isAuthenticated = data;
    });
  }

}
