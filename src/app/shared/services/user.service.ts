import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { UserModel, Role, Permission } from 'src/app/portal/models/users';
import { GlobalConstants} from 'src/app/shared/services/global-settings.service';
//import { User_Mock } from 'src/app/portal/models/user.mockdata';


@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  ///--------------------------------------------------------------------------
  /// Get User by Id
  ///--------------------------------------------------------------------------
  getItem_ById(id: any): Observable<UserModel> {

    //TBD - get this working where post vars can be passed in as param body
    //return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUserByID', { userID: id})
    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUserByID?userID=' + id, { userID: id })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return user object
          var result: UserModel = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Get User by Id
  ///--------------------------------------------------------------------------
  getItems_ByPartnerId(partnerId: any): Observable<Array<UserModel>> {

    //TBD - get this working where post vars can be passed in as param body
    //return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUserByID', { userID: id})
    return this.http.post<any>(GlobalConstants.BASE_API_URL + 'User/GetUsersByPartnerID?partnerID=' + partnerId, { partnerID: partnerId })
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return user array object
          var result: Array<UserModel> = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Get All
  ///--------------------------------------------------------------------------
  getPermissions_All(): Observable<Array<Permission>> {

    return this.http.get<any>(GlobalConstants.BASE_API_URL + 'permission/getall', {})
      .pipe(map((res: any) => {
        // call successful if there's a jwt token in the response
        if (res) {
          //return data
          var result: Array<Permission> = res;
          return result;
        }
      }));
  }

  ///--------------------------------------------------------------------------
  /// Save item
  ///--------------------------------------------------------------------------
  saveItem(item: UserModel): Observable<UserModel> {

    var url: string = GlobalConstants.BASE_API_URL + 'user/' + (item.id == undefined ? 'add' : 'update');
    return this.http.post<UserModel>(url, item)
      .pipe(map((res: UserModel) => {
        var result: UserModel = null;
        if (res) {
          result = res;
        }
        console.log(res);
        return result;
      //  console.log(res);
      //  return res.indexOf('success') != -1;
      }));
  }
}

var role: Role;
var permissions: Array<any>;

// Get the cuurent role for this user, which is User[0]; 
/*
function setRole() {
  var selectedrole = User_Mock[0].role;

  if (this.isEmptyObject(role)) {
     console.log('No Role found');
  } else {
     localStorage.setItem('role', JSON.stringify(role));
     console.log("role id is:", selectedrole.roleid);
     console.log("role name is", selectedrole.name);
  }
}

function getPermissions() {
  var selectedpermissions = this.User_Mock[0].permissions; 

  if (this.isEmptyObject(selectedpermissions)) {
     console.log('No permissions found');
  } else {
     localStorage.setItem('permissions', JSON.stringify(selectedpermissions));
     console.log("permissions are", selectedpermissions[0]);
  }
  return selectedpermissions;
}

//Given a User return its role object
function getRole() {
  
  var selectedRole = this.User_Mock.map(function (UserModel) {
     return UserModel.role; 
  });

  if (this.isEmptyObject(role)) {
     console.log('No role found for id: ", id');
  } else {
        console.log('role returned is:', selectedRole[0]);
     return selectedRole;
  }
}

function isEmptyObject(obj) {
  for (var prop in obj) {
     if (obj.hasOwnProperty(prop)) {
        return false;
     }
  }

}

setRole();
getPermissions();
getRole();
*/
