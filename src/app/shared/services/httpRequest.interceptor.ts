import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserToken } from 'src/app/portal/models/user-token';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { GlobalConstants } from 'src/app/shared/services/global-settings.service';

var CLASS_NAME: string = "HttpRequestInterceptor";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  constructor(private svcUtility: UtilityService, private svcAuthenticate: AuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with bearer auth credentials if available
    // note the initial login will not have this header
    let tokenVal = JSON.parse(localStorage.getItem('userToken'));
    if (tokenVal && tokenVal.token) {
      request = request.clone({
        setHeaders: {
          'Authorization': `Bearer ${tokenVal.token}`,
          'Content-Type': 'application/json'
        }
      });
    }
    else {
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json'
        }
      });
    }

//    return next.handle(request);

    return <any>next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        //for unauthroized, log user out
        if (error.status == 401) {
          //trying to limit dependencies of this class on other classes so doing stuff here instead of calling common service.
          this.svcUtility.logMessage(error, CLASS_NAME, this.svcUtility.LogMessageSeverity.Error);
          //show user a timed out message if they had a user token and then get unauthroized
          var msg: string = "You are not authorized to view this page. Please login and try again.";
          if (localStorage.getItem('userToken') != null)
          {
            msg = GlobalConstants.SessionTimeoutMessage;
          }
          this.svcUtility.displayMessage(msg, this.svcUtility.LogMessageSeverity.Warn);
          //clear local storage to ensure log out
          this.svcAuthenticate.logout();
          //localStorage.removeItem('currentUser');
          //localStorage.removeItem('userToken');
          //redirect route to login page
          //this.router.navigate(["/login"]);
        }
        return throwError(error);
      }));



  }
}
