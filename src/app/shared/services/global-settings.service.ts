import { Injectable, isDevMode } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';

///--------------------------------------------------------------------------
/// Download format enum
///--------------------------------------------------------------------------
export enum ReportDownloadFormat {
    PDF = 'pdf'
  , CSV = 'csv'
}

export enum AppPermissionEnum
{
  CanManagePartners = "CanManagePartners",
  CanManageAllUsers = "CanManageAllUsers",
  CanManageUsers = "CanManageUsers",
  CanManageRoles = "CanManageRoles",
  CanViewFinancialReports = "CanViewFinancialReports",
  CanViewSettlementReports = "CanViewSettlementReports",
  CanViewMarketingReports = "CanViewMarketingReports"
}

//purely static settings that remain unchanged during the lifecycle
export const GlobalConstants = {
    //BASE_API_URL: 'https://localhost:44356/api/' 
    BASE_API_URL: 'https://vdc3plreportportalapi.azurewebsites.net/api/'
  , TitleAnonymous: 'Vitamin Discount Center Report Portal'
  , Title: 'Vitamin Discount Center Report Portal'
  , SessionLength: 1000 * 60 * 30 //30 mins in milliseconds
  , PageSize: 25 
  , ReportPageSize: 100 
  , PageSizeOptions: [25, 50, 75, 100, 200] 
  , DateFormat: 'M/d/yyyy' 
  , DateFormat_Grid: 'MM/dd/yyyy' 
  , SelectOneCaption: '--Select One--'
  , ReportDownloadFormat: ReportDownloadFormat
  , AppPermissionsEnum: AppPermissionEnum
  , SessionTimeoutMessage: "Your session has timed out. Please log in to continue."
  //datepicker options
  , DatePickerOptions: {
      minYear: 2019,
      maxYear: 2030,
      displayFormat: 'M/D/YYYY',
      barTitleFormat: 'MMM YYYY',
      dayNamesFormat: 'dd',
      firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
      locale: enLocale,
      //minDate: new Date(Date.now()), // Minimal selectable date
      maxDate: new Date(Date.now()),  // Maximal selectable date
      barTitleIfEmpty: 'Select date',
      placeholder: 'M/d/yyyy',
      addClass: 'form-control vdc-datepicker-input',
      //addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
      //fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
      useEmptyBarTitle: false
    }
};

//global variables that can change during the user session
@Injectable()
export class GlobalVariables {
}
