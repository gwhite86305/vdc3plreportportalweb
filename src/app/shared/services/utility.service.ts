import { Injectable, isDevMode } from '@angular/core';
import { LogMessagenModel, LogMessageNew, LoggerSeverity } from 'src/app/shared/interfaces/logMessage.interface';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { FormControl} from '@angular/forms';
import { ReportSortModel } from 'src/app/portal/models/report.model';
import { DisplayMessageService } from 'src/app/shared/services/display-message.service';

const CLASS_NAME: string = 'UtilityService'; 

///--------------------------------------------------------------------------
/// UtilityService - used for common, utility activities across application
///--------------------------------------------------------------------------
@Injectable()
export class UtilityService {

  private _logMessageDelimiter: string = " || ";

  constructor(private svcDisplayMessage: DisplayMessageService) { }
  ///#region: Logging Helper Methods
  ///--------------------------------------------------------------------------
  /// Get the enum for log message severity. Do this as a wrapper around the actual enum
  /// so that each component doesn't need to add a reference to LoggerServerity.
  /// Basically, simplify implementation for each component. 
  ///--------------------------------------------------------------------------
  get LogMessageSeverity() {
    return LoggerSeverity;
  }
  ///--------------------------------------------------------------------------
  /// Generate Log Message String. This allows components to use this common message
  /// format and log the message directly to the console so they can have line number and
  /// file where message is generated.
  /// Sample:
  ///  console.log(this.svcUtility.getlogMessageString("ngOnInit", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info));
  ///--------------------------------------------------------------------------
  getlogMessageString(message: string, category: string = '', severity: LoggerSeverity = LoggerSeverity.Info) {
    var msg: LogMessagenModel = { Code: '', Message: message, Category: category, Severity: severity, DateCreated: new Date() };
    return this.getlogMessageStringByModel(msg);
  }

  private getlogMessageStringByModel(msg: LogMessagenModel) {
    return this.getTimeStamp(msg.DateCreated)
      + (msg.Category != '' ? this._logMessageDelimiter : '') + msg.Category
      + this._logMessageDelimiter + msg.Severity.toString()
      + (msg.Code != '' ? this._logMessageDelimiter : '') + msg.Code
      + (msg.Message != '' ? this._logMessageDelimiter : '') + msg.Message;
  }

  ///--------------------------------------------------------------------------
  /// Log a formatted message
  /// Sample:
  ///  this.svcUtility.logMessage("ngOnInit", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
  ///--------------------------------------------------------------------------
  public logMessage(message: any, category: string = '', severity: LoggerSeverity = LoggerSeverity.Info) {
    if (!isDevMode) return;
    //convert error object to string
    if (typeof message === "string") {//do nothing
    }
    else { message = JSON.stringify(message); }

    var msg: LogMessagenModel = { Code: '', Message: message, Category: category, Severity: severity, DateCreated: new Date()};
    this.logMessageByModel(msg);
  }
  ///--------------------------------------------------------------------------
  /// Log a formatted message using the logMessage model as param
  ///--------------------------------------------------------------------------
  private logMessageByModel(msg: LogMessagenModel) {
    if (!isDevMode) return;
    var formattedMsg = this.getlogMessageStringByModel(msg);
    switch (msg.Severity) {
      case LoggerSeverity.Error:
        console.error(formattedMsg);
        break;
      case LoggerSeverity.Warn:
        console.warn(formattedMsg);
        break;
      case LoggerSeverity.Debug:
        console.debug(formattedMsg);
        break;
      default:
        console.log(formattedMsg);
    }
  }

  ///--------------------------------------------------------------------------
  /// Log an elapsed time message. This is useful when trying to get elapsed time
  /// for an action between two times. 
  /// Sample:
  ///    this.svcUtility.logMessageElapsedTime("ngOnInit", CLASS_NAME, new Date('2018-01-21'), new Date());
  ///--------------------------------------------------------------------------
  public logMessageElapsedTime(message: string, category: string, startDate: Date, endDate: Date) {
    if (!isDevMode) return;
    var elapsed = endDate.getTime() - startDate.getTime();
    var elapsedMessage: string = 'Elasped Time: ' + elapsed + ' (Start: ' + this.getTimeStamp(startDate) +
      ', End: ' + this.getTimeStamp(endDate) + ')';
    var msg: LogMessagenModel = {
      Code: '', Message: message + this._logMessageDelimiter + elapsedMessage,
      Category: category, Severity: LoggerSeverity.Info, DateCreated: endDate
    };
    this.logMessageByModel(msg);
  }

  ///--------------------------------------------------------------------------
  /// Create a timestamp value in a consistent manner
  ///--------------------------------------------------------------------------
  private getTimeStamp(d: Date) {
    var result = (d.getHours() < 10 ? '0' : '') + d.getHours() + ':' +
      (d.getMinutes() < 10 ? '0' : '') + d.getMinutes() + ':' +
      (d.getSeconds() < 10 ? '0' : '') + d.getSeconds() + ':' +
      (d.getMilliseconds() < 10 ? '00' : (d.getMilliseconds() < 100 ? '0' : '')) + d.getMilliseconds();
    return result;
  }
  ///#endregion: Logging Helper Methods

  ///--------------------------------------------------------------------------
  /// Display friendly error message to user on screen
  ///--------------------------------------------------------------------------
  displayMessage(message: string, severity: LoggerSeverity = LoggerSeverity.Info) {
    this.svcDisplayMessage.addDisplayMessage(message, severity);
  }
  ///--------------------------------------------------------------------------
  /// Show/hide system processing indicator
  ///--------------------------------------------------------------------------
  setSystemProcessing(flag: boolean) {
    this.svcDisplayMessage.setSystemProcessing(flag);
  }

  ///--------------------------------------------------------------------------
  /// Translate http error codes into friendly messages
  ///--------------------------------------------------------------------------
  translateHTTPError(err: HttpErrorResponse) {

    //if (isDevMode) return 'Error: ' + error.message; //return raw message for devs
    var msgGeneric: string = 'An error has occurred. Please try again. If the error continues, please contact your system administrator.';

    if (err.status != null) {
      switch (err.status)
      {
        case 400:
          return err.error;
        case 500:
          return msgGeneric;
        case 501:
          return "This action is under construction.";
        default:
          if (err.error == null) return err.error;
          return err.message;
      }
    }
    //if we get here, return a generic message. 
    return msgGeneric;
  }

  ///--------------------------------------------------------------------------
  /// Form Utility helpers
  ///--------------------------------------------------------------------------
  validateFormField_Required(fld: FormControl)
  {
    return fld.errors && fld.errors.required && (fld.dirty || fld.touched);
  }

  validateFormSelectField_Required(fld: FormControl) {
    return fld.errors && fld.errors.invalidSelect && (fld.dirty || fld.touched);
  }

  ///--------------------------------------------------------------------------
  /// Grid Utility helpers
  ///--------------------------------------------------------------------------
  setSortIndicator(sort: ReportSortModel, colName: string) {
    //set sort indicator based on current sort condition
    if (sort.colName == colName) {
      return sort.isDescending ? "fa fa-sort-down" : "fa fa-sort-up";
    }
    return "none";
  }

  //Perform the sort AND update the sort indicator data
  sortColumn(sort: ReportSortModel, data: Array<any>, colName: string) {
    //TODO: support date type as a sort type 
    //defer updating visual display till data is fully sorted. sort model val
    var tempSort = JSON.parse(JSON.stringify(sort));
    if (tempSort.colName != colName) {
      tempSort.colName = colName;
      tempSort.isDescending = false;
    }
    else if (tempSort.colName == colName) {
      tempSort.isDescending = !tempSort.isDescending;
    }

    var arrSorted: Array<any> = JSON.parse(JSON.stringify(data));
    arrSorted = arrSorted.sort(function (a, b) {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      [propertyA, propertyB] = [a[colName], b[colName]];

      const valueA = isNaN(+propertyA) ? propertyA.toString().toLowerCase() : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB.toString().toLowerCase() : +propertyB;

      return (valueA < valueB ? -1 : 1) * (tempSort.isDescending ? -1 : 1);
    });

    return {
      sort: tempSort, data: arrSorted
    };
  }

  //init the pager object
  initGridPager(totalRowCount: number, pageSize: number): Array<number> {
    var result: Array<number> = [];
    if (totalRowCount > pageSize) {
      var numPages = Math.ceil(totalRowCount / pageSize);
      for (var i = 1; i <= numPages; i++) {
        result.push(i);;
      }
    }
    return result;
  }


}
