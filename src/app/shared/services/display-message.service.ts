import { Injectable, isDevMode } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { DisplayMessagenModel } from 'src/app/shared/interfaces/displayMessage.interface';
import { LoggerSeverity } from 'src/app/shared/interfaces/logMessage.interface';

const CLASS_NAME: string = 'DisplayMessageService';

///--------------------------------------------------------------------------
/// ErrorService - used for display of error messages in a global manner
///--------------------------------------------------------------------------
@Injectable({ providedIn: 'root' })
export class DisplayMessageService {

  ///--------------------------------------------------------------------------
  /// Is Loading observable - A global is loading observable will be listened to.
  ///   When true, show indicator on the screen.
  ///--------------------------------------------------------------------------
  isProcessing: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isSystemProcessing());
  private isSystemProcessing(): boolean {
    //get from local storage
    if (localStorage.getItem('isSystemProcessing') != null) {
      return localStorage.getItem('isSystemProcessing') == 'true';
    }
    return false;
  }

  ///--------------------------------------------------------------------------
  /// Set is processing flag
  ///--------------------------------------------------------------------------
  setSystemProcessing(flag: boolean) {
    //update storage
    localStorage.setItem('isSystemProcessing', flag.toString().toLowerCase());
    this.isProcessing.next(flag);
  }


  ///--------------------------------------------------------------------------
  /// Error observable - A global error observable will be listened to. when the
  ///   error message changes, it will display the error in a common location on screen.
  //    This is to handle the display side of things. 
  ///--------------------------------------------------------------------------
  currentMessages: BehaviorSubject<Array<DisplayMessagenModel>> = new BehaviorSubject<Array<DisplayMessagenModel>>(this.getCurrentMessages());
  private getCurrentMessages(): Array<DisplayMessagenModel> {
    //get from local storage
    if (localStorage.getItem('currentMessages') != null) {
      var result: Array<DisplayMessagenModel> = JSON.parse(localStorage.getItem('currentMessages'));
      return result;
    }
    return null;
  }

  ///--------------------------------------------------------------------------
  /// Append error to errors collection
  ///--------------------------------------------------------------------------
  addDisplayMessage(val: string, severity: LoggerSeverity) {
    //set in local storage, append value so that if multiple errors occur in multiple locations, we don't lose anything, update observable
    var result: Array<DisplayMessagenModel> = this.getCurrentMessages();
    if (result == null) result = [];

    var msg: DisplayMessagenModel = { Message: val, Severity: severity, DateCreated: new Date() };

    //don't keep adding same error message if already present
    if (result.length == 0) result.push(msg);
    else {
      var hasMatch: boolean = result.some((item: DisplayMessagenModel) => {
        if (item.Message.toLowerCase() == val.toLowerCase() && item.Severity == severity) return true;
      });
      if (!hasMatch) result.push(msg);
    }
    //update storage
    localStorage.setItem('currentMessages', JSON.stringify(result));
    this.currentMessages.next(result);
  }

  ///--------------------------------------------------------------------------
  /// Clear messages of a certain type
  ///--------------------------------------------------------------------------
  clearDisplayMessages(severity: LoggerSeverity) {
    //clear local storage of one type of severity
    //set in local storage, append value so that if multiple errors occur in multiple locations, we don't lose anything, update observable
    var result: Array<DisplayMessagenModel> = this.getCurrentMessages();
    if (result == null) {
      this.currentMessages.next(null);
      return;
    }

    //don't keep adding same error message if already present
    result = result.filter(function (msg: DisplayMessagenModel) {
      return msg.Severity != severity;
    });

    //update storage
    localStorage.setItem('currentMessages', JSON.stringify(result));
    this.currentMessages.next(result);
  }

  ///--------------------------------------------------------------------------
  /// Clear ALL messages 
  ///--------------------------------------------------------------------------
  clearDisplayMessagesAll() {
    //update storage
    localStorage.setItem('currentMessages', null);
    this.currentMessages.next(null);
  }

}
