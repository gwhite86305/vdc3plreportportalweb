import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppMessageComponent } from 'src/app/shared/app-message/app-message.component';
import { AppSpinnerComponent } from 'src/app/shared/app-spinner/app-spinner.component';

@NgModule({
  imports: [RouterModule, CommonModule],
  declarations: [NavbarComponent, HeaderComponent, AppMessageComponent,AppSpinnerComponent],
  exports: [NavbarComponent, HeaderComponent, AppMessageComponent, AppSpinnerComponent]
})
export class SharedModule { }
