import { Component, Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { DisplayMessageService } from 'src/app/shared/services/display-message.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './app-spinner.component.html',
  styleUrls: ['./app-spinner.component.scss']
})

@Injectable()
export class AppSpinnerComponent implements OnInit, OnDestroy {
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  isProcessing: boolean;

  constructor(
    private svcDisplayMessages: DisplayMessageService ) { }

  ngOnInit() {
    this.svcDisplayMessages.isProcessing.subscribe(data => {
      this.isProcessing = data;
    }); //.takeUntil(this.destroyed$);
  }

  ngOnDestroy() {
    console.log('on destroy');
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
