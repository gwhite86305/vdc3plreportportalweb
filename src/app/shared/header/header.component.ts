import { Component, Injectable, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/portal/models/users';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

@Injectable()
export class HeaderComponent implements OnInit {
  isAuthenticated: Observable<boolean>;
  user: UserModel;

  constructor(
    private svcAuthenticate: AuthenticationService,
    private svcUtility: UtilityService
  ) { }

  ngOnInit() {
    //this.isAuthenticated = this.svcAuthenticate.isAuthenticated;
    this.svcAuthenticate.currentUser.subscribe(data => {
      //assign the value to the user object used in the display
      this.user = data;
    });
  }

  logout() {
    this.svcAuthenticate.logout();
  }
}
