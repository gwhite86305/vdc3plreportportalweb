import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { GlobalConstants } from 'src/app/shared/services/global-settings.service';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/portal/services/authentication.service';
import { UtilityService } from 'src/app/shared/services/utility.service';

var CLASS_NAME: string = "AppComponent";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  //isAuthenticated: Observable<boolean>;
  title = GlobalConstants.Title;

  constructor(
    private svcAuthenticate: AuthenticationService,
    private svcUtility: UtilityService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    console.log(this.svcUtility.getlogMessageString("constructor", CLASS_NAME, this.svcUtility.LogMessageSeverity.Info));
  }

  ngOnInit() {
    this.svcAuthenticate.isAuthenticated.subscribe(data => {
      //if not authenticated, call logout
      if (!data) {
        this.svcUtility.logMessage('isAuthenticated changed...logging out', CLASS_NAME, this.svcUtility.LogMessageSeverity.Info);
        this.svcUtility.displayMessage(GlobalConstants.SessionTimeoutMessage, this.svcUtility.LogMessageSeverity.Warn);
        //TBD - reload current route will trigger route guard to go to login with return url
        //this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/home']);
        //if (this.route.snapshot.url != '') {
        //  this.router.navigate(['/login'], { queryParams: { returnUrl: this.route.snapshot.url } });
        //}
        //else {
          this.router.navigate(["/login"]);
        //}
      }
    });
  }

}
