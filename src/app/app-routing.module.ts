import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';

const routes: Routes = [
  { path: '', loadChildren: './portal/portal.module#PortalModule', canActivate: [AuthGuard] }
  , { path: 'login', component: LoginComponent}
  , { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [AuthGuard]  }
  , { path: 'portal', loadChildren: './portal/portal.module#PortalModule', canActivate: [AuthGuard]  }
  , { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  //, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
